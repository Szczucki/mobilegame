package com.mobilegame;

import com.mobilegame.objects.GameState;
import com.mobilegame.objects.Player;
import com.mobilegame.views.PrimaryView;
import com.mobilegame.views.SecondaryView;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.visual.Swatch;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TouchEvent;
import javafx.stage.Stage;


public class Mobilegame extends MobileApplication {

    public static final String PRIMARY_VIEW = HOME_VIEW;
    public static final String SECONDARY_VIEW = "Secondary View";
GameState gameState = GameState.getInstance();

    static final Image zombie = new Image("/resources/shambling-zombie.jpg");
    static final Image vampire = new Image("/resources/vampire.jpg");

    static final Image ghost = new Image("/resources/ghost.jpg");

    @Override
    public void init() {
        addViewFactory(PRIMARY_VIEW, () -> new PrimaryView().getView());
        addViewFactory(SECONDARY_VIEW, () -> new SecondaryView().getView());

        DrawerManager.buildDrawer(this);
    }

    @Override
    public void postInit(Scene scene) {
        Swatch.BLUE.assignTo(scene);

        scene.getStylesheets().add(Mobilegame.class.getResource("style.css").toExternalForm());
        ((Stage) scene.getWindow()).getIcons().add(new Image(Mobilegame.class.getResourceAsStream("/icon.png")));

    }

    public static void endGame() {

    }

}
