package com.mobilegame.objects;

import com.mobilegame.Config;
import javafx.scene.image.Image;

import static com.mobilegame.Config.WIDTH;

/**
 * Created by tomasz.szczucki on 15.04.2020.
 */
public class Zombie extends Enemy{

    private static int SIZE = 20;
    private static Image IMAGE = new Image("/resources/wolfie.jpg");

    public Zombie() {
        super(50 + RAND.nextInt(WIDTH - 100), 0, SIZE, IMAGE);
        this.speed = Config.ZOMBIE_SPEED;
        this.strength = Config.ZOMBIE_STRENGTH;
        this.hp = Config.ZOMBIE_HP;
    }
}
