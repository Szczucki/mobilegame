package com.mobilegame.objects;

/**
 * Created by tomasz.szczucki on 14.04.2020.
 */
public interface Destructible {

    void destroy ();
}
