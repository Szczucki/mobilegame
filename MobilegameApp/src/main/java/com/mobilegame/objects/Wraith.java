package com.mobilegame.objects;

import com.mobilegame.Config;
import javafx.scene.image.Image;


/**
 * Created by tomasz.szczucki on 15.04.2020.
 */
public class Wraith extends  Enemy {

    private static int SIZE = 20;
    private static Image IMAGE = new Image("/resources/wolfie.jpg");

    public Wraith() {
        super(0, 50 + RAND.nextInt(Config.HEIGHT - 100), SIZE, IMAGE);
        this.speed = Config.WRAITH_SPEED;
        this.strength = Config.WRAITH_STRENGTH;
        this.hp = Config.WRAITH_HP;
    }
}
