package com.mobilegame.objects;


import com.mobilegame.Config;

public class GameState {


        private GameState() {

        }

        private static GameState gameStateInstance;

        public static GameState getInstance() {
            if (gameStateInstance == null) {
                synchronized (GameState.class) {
                    if (gameStateInstance == null) {
                        gameStateInstance = new GameState();
                    }
                }
            }
            return gameStateInstance;
        }

    Config config;
    Player player;


    public Config getConfig() {
        return config;
    }

    public Player getPlayer() {
        return player;
    }

    public void setConfig(Config config) {
        this.config = config;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }


}
