package com.mobilegame.objects;

import com.mobilegame.Mobilegame;

/**
 * Created by tomasz.szczucki on 14.04.2020.
 */
public class Player {

    String name;
    int hp;
    int gold;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public void takeDamage(int damage) {
        hp -= damage;
        if (hp <= 0) {
            Mobilegame.endGame();
        }
    }
}
