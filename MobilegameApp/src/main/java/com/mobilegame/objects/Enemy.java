package com.mobilegame.objects;

import com.mobilegame.Config;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.util.List;
import java.util.Random;

import static com.mobilegame.Config.WIDTH;

/**
 * Created by tomasz.szczucki on 14.04.2020.
 */
public class Enemy extends GameObject {


    GraphicsContext gc;
    protected static final Random RAND = new Random();
    int hp;

    List weakness;
    Image image;
    int strength;


    public Enemy(int posX, int posY, int size, Image image) {
        super(posX, posY, size, image);
    }

    public void draw() {
        gc.drawImage(image, posX, posY, size, size);
    }

    public void update() {
        move();
    }

    private void move() {
        if (!destroyed) {
            posY -= speed;
        }
        if (posX > Config.WIDTH) {
            dealDamage();
            destroy();
        }
    }

    void dealDamage() {
        GameState.getInstance().getPlayer().takeDamage(strength);
    }
}
