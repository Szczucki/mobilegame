package com.mobilegame.objects;

import com.mobilegame.Config;
import javafx.scene.image.Image;

/**
 * Created by tomasz.szczucki on 14.04.2020.
 */
public class GameObject {

    int posY, posX, size, speed;
    boolean destroyed;
    Image image;

    public GameObject() {
        super();
    }

    public GameObject(int posX, int posY, int size, Image image) {
        this.posX = posX;
        this.posY = posY;
        this.size = size;
        this.image = image;
    }

    public boolean collide(GameObject object) {
        int dist = calcDistance(this.posX + this.size / 2, this.posY + this.size /2,
                object.posX + object.size / 2, object.posY + object.size /2);
        return dist < (this.size /2 + object.size /2);
    }

    public int calcDistance(int x1, int y1, int x2, int y2) {
        return (int) Math.sqrt(Math.pow((x1 - x2), 2) + Math.pow((y1 - y2),2));
    }



    public void destroy () {
        destroyed = true;
    }
}
