package com.mobilegame.objects;

import java.util.List;

/**
 * Created by tomasz.szczucki on 16.04.2020.
 */
public class CurrentLevel {

    private List<Enemy> enemies;
    private int points;
    private boolean levelOver;

    private CurrentLevel() {

    }

    private static CurrentLevel currenLevelInstance;

    public static CurrentLevel getInstance() {
        if (currenLevelInstance == null) {
            synchronized (CurrentLevel.class) {
                if (currenLevelInstance == null) {
                    currenLevelInstance = new CurrentLevel();
                }
            }
        }
        return currenLevelInstance;
    }

    public List<Enemy> getEnemies() {
        return enemies;
    }

    public void setEnemies(List<Enemy> enemies) {
        this.enemies = enemies;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public Enemy spawnEnemy() {
        Enemy enemy;
        enemy = new Zombie();
        return enemy;
    }

    public boolean isLevelOver() {
        return levelOver;
    }

    public void setLevelOver(boolean levelOver) {
        this.levelOver = levelOver;
    }
}
