package com.mobilegame.objects;

import javafx.scene.image.Image;

import java.util.List;

/**
 * Created by tomasz.szczucki on 14.04.2020.
 */
public class Bullet extends GameObject {

    public static int DAMAGE_TYPE_SILVER = 0;
    public static int DAMAGE_TYPE_WOOD = 1;
    public static int DAMAGE_TYPE_BLESSED = 2;
    public static int DAMAGE_TYPE_PLASMA = 3;

    List<String> damageTypes;
    int dmg;
    Image image;

    public Bullet (List<String>  damageTypes, int dmg) {
        this.damageTypes = damageTypes;
        this.dmg = dmg;
    }


}
