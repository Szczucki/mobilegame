package com.mobilegame.views;

import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.NavigationDrawer;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.mobilegame.objects.GameState;
import com.mobilegame.objects.Player;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

import static com.mobilegame.Mobilegame.SECONDARY_VIEW;

public class PrimaryPresenter {

    @FXML
    private View primary;

    @FXML
    private Label label;

    final NavigationDrawer.Item playItem = new NavigationDrawer.ViewItem("Secondary", MaterialDesignIcon.DASHBOARD.graphic(), SECONDARY_VIEW);

    public void initialize() {
        primary.showingProperty().addListener((obs, oldValue, newValue) -> {
            if (newValue) {
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setNavIcon(MaterialDesignIcon.MENU.button(e -> 
                        MobileApplication.getInstance().getDrawer().open()));
                appBar.setTitleText("Primary");
                appBar.getActionItems().add(MaterialDesignIcon.SEARCH.button(e -> 
                        System.out.println("Search")));
            }
        });
    }
    
    @FXML
    void buttonClick() {
        initGameState();
        MobileApplication.getInstance().switchView("SECONDARY_VIEW");
    }

    private void initGameState() {
        GameState gameState = GameState.getInstance();
        Player newPlayer = new Player();
        newPlayer.setGold(100);
        newPlayer.setHp(100);
        gameState.setPlayer(newPlayer);
    }
    
}
