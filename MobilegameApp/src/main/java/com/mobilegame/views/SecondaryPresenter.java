package com.mobilegame.views;

import com.gluonhq.charm.glisten.animation.BounceInRightTransition;
import com.gluonhq.charm.glisten.application.MobileApplication;
import com.gluonhq.charm.glisten.control.AppBar;
import com.gluonhq.charm.glisten.control.FloatingActionButton;
import com.gluonhq.charm.glisten.mvc.View;
import com.gluonhq.charm.glisten.visual.MaterialDesignIcon;
import com.mobilegame.Config;
import com.mobilegame.objects.CurrentLevel;
import com.mobilegame.objects.Enemy;
import com.mobilegame.objects.GameObject;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class SecondaryPresenter {

    CurrentLevel currentLevel = CurrentLevel.getInstance();
    GraphicsContext gc;

    @FXML
    private View secondary;
    double mouseX, mouseY;

    public void initialize() {
        secondary.setShowTransitionFactory(BounceInRightTransition::new);

        FloatingActionButton fab = new FloatingActionButton(MaterialDesignIcon.INFO.text,
                e -> System.out.println("Info"));
        fab.showOn(secondary);

        secondary.showingProperty().addListener((obs, oldValue, newValue) -> {
            if (newValue) {
                AppBar appBar = MobileApplication.getInstance().getAppBar();
                appBar.setNavIcon(MaterialDesignIcon.MENU.button(e ->
                        MobileApplication.getInstance().getDrawer().open()));
                appBar.setTitleText("Secondary");
                appBar.getActionItems().add(MaterialDesignIcon.FAVORITE.button(e ->
                        System.out.println("Favorite")));
            }
        });
    }

    @FXML
    void buttonClick() {
        initGameObjects();
        MobileApplication.getInstance().switchView("SECONDARY_VIEW");
    }

    private void initGameObjects() {

        currentLevel.setPoints(0);
        currentLevel.setEnemies(new ArrayList<>());

        IntStream.range(0, 40).mapToObj(i -> currentLevel.spawnEnemy()).forEach(currentLevel.getEnemies()::add);
    }

    public void start(Stage stage) {
        Canvas canvas = new Canvas(Config.WIDTH, Config.HEIGHT);
        gc = canvas.getGraphicsContext2D();
        Timeline timeline = new Timeline(new KeyFrame(Duration.millis(100), e -> tick(gc)));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
        canvas.setOnMouseMoved(e -> {
            mouseX = e.getX();
            mouseY = e.getY();
        });
        canvas.setOnMouseClicked(e -> {
            GameObject hitMarker = new GameObject((int)mouseX, (int)mouseY, 6, null);
            currentLevel.getEnemies().forEach(o -> {
                if (o.collide(hitMarker)){
                    o.destroy();
                }
            });
            hitMarker.destroy();
        });

    }

    //Operations of this method indicate the basic unit of game time (one "tick")
    private void tick(GraphicsContext gc) {
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setFont(Font.font(20));
        gc.setFill(Color.WHITE);
        gc.fillText("Score: " + currentLevel.getPoints(), 60, 20 );

        if(currentLevel.isLevelOver()) {
            gc.setFont(Font.font(35));
            gc.setFill(Color.CRIMSON);
            gc.fillText("You are as dead as they come. Value engraved on your tombstone is: " + currentLevel.getPoints(), Config.WIDTH /2, Config.HEIGHT /2);
        }

        currentLevel.getEnemies().stream().peek(Enemy::update).peek(Enemy::draw);
//        .forEach(e -> {if (player)});
    }


}
