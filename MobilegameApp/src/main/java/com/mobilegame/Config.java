package com.mobilegame;

/**
 * Created by tomasz.szczucki on 14.04.2020.
 */
public interface Config {

    int HEIGHT = 600;
    int WIDTH = 800;

    int WEREWOLF_SPEED = 10;
    int WEREWOLF_STRENGTH = 6;
    int WEREWOLF_HP = 1;

    int VAMPIRE_SPEED = 15;
    int VAMPIRE_STRENGTH = 3;
    int VAMPIRE_HP = 1;

    int WRAITH_SPEED = 10;
    int WRAITH_STRENGTH = 3;
    int WRAITH_HP = 1;

    int ZOMBIE_SPEED = 5;
    int ZOMBIE_STRENGTH = 3;
    int ZOMBIE_HP = 2;

}
